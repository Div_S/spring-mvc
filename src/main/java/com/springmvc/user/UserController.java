package com.springmvc.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class UserController {
     
	@Autowired
	UserService service;
	@Autowired
	LoginController login;
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String showSignupPage() {
		return "signup";
	}
	
	@RequestMapping(value="/signup",method=RequestMethod.POST)
	public String signup(User user)
	{
		service.addUser(user.getName(), user.getEmail(), user.getPassword());
		System.out.println("User Added");
		System.out.println(service.retrieveUsers());
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginPage() {
		return "login";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String Login(@RequestParam String email,@RequestParam String password)
	{
		boolean isvalid=login.isValid(email, password);
		if(!isvalid)
		{
			return "redirect:/login";
		}
		return "welcome";
	}
	
	
	

}
