package com.springmvc.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class UserService {
 private static List<User> users = new ArrayList<User>();
 private static int count=0;
 
 public void addUser(String name,String email,String password)
 {
	 
	 users.add(new User(name,count++,email,password));
 }

 public List<User> retrieveUsers(){
	 
	 return users;
	}
}
